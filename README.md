# OpenML dataset: Currency-Exchange-Rates

https://www.openml.org/d/43506

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset contains the daily currency exchange rates as reported to the International Monetary Fund by the issuing central bank. Included are 51 currencies over the period from 01-01-1995 to 11-04-2018.
The format is known as currency units per U.S. Dollar. Explained by example, each rate in the Euro column says how much U.S. Dollar you had to pay at a certain date to buy 1 Euro. Hence, the rates in the column U.S. Dollar are always 1.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43506) of an [OpenML dataset](https://www.openml.org/d/43506). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43506/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43506/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43506/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

